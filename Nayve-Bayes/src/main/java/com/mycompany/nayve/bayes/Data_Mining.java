/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.nayve.bayes;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author carlo
 */
public class Data_Mining {

    Data_Warehouse dados = new Data_Warehouse();
    Data_Bayes bayes = new Data_Bayes();
    Tratamento tratamento = new Tratamento();
    ResultSet name = bayes.RetiraNomes();
    public void Splitter() throws SQLException {
        ResultSet resultset, total;
        dados.criar();
        int tam = 0, init = 1;

        resultset = dados.select();
        total = dados.select();
        while (total.next()) {
            tam++;
            //meramente ilustrativo para verificar o que ta acontecendo no algoritimo.
        }
        total.close();
        while (resultset.next()) {
            String prividencia = resultset.getString("decisao");        
            String[] palavras = prividencia.split(" ");
            System.out.println("Linha: " + init + " de total:" + tam);
            for (int i = 0; i < palavras.length; i++) {
                palavras[i] = palavras[i].replaceAll("[0-9]", "");
                palavras[i] = tratamento.tratamento(palavras[i]);
                if (palavras[i].length() < 4 || bayes.chaveOK(palavras[i])) {

                } else {                    
                    bayes.inserir(palavras[i]);
                }

            }
            init++;
        }

    }

}
