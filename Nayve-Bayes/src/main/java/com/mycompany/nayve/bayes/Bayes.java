/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.nayve.bayes;

import Model.Probabilidades;
import Model.Termos;
import java.util.List;

/**
 *
 * @author carlos && Eduardo
 *
 */
public class Bayes {

    /**
     *
     * @param eventos Termos eventos - Contém a Chave encontrada no Documento, o
     * nº de ocorencias para cada classificação catalogada e a Probabilidade
     * para cada uma dela ocorrer
     * @param Probabilidades - Contém as probabilidades de cada classificação
     * ocorrer dentro do universo testado
     * @return Retorna a nova probabilidade para ser usada para os calculos
     * posteriores
     */
    public double[] calculo(Probabilidades Pb, List<Termos> eventos) {
        double[] Pabxb = new double[22];
        double[] decis = new double[22];

        // P(a/b)*b
        System.out.println("P(a/b)*b");
        for (int i = 0; i < 22; i++) {
            System.out.print(Pab(eventos)[i] + "*" + Pb.getProbabilidade()[i] + " || ");
            Pabxb[i] = Pab(eventos)[i] * Pb.getProbabilidade()[i];
        }
        double pb = Pb(Pabxb);

        // Aqui é o bayes 
        System.out.print("\n");
        for (int i = 0; i < 22; i++) {
            System.out.print(Pabxb[i] + "/" + pb + "||");
            decis[i] = Pabxb[i] / pb;
        }

        // Nova.setProbabilidade(temp);
        return decis;
    }

    /**
     *
     * @param eventos
     * @return Cada posição do vetor representa P(A/B)
     */
    private double[] Pab(List<Termos> eventos) {
        double[] pab = new double[22];
        int init = 0;
        while (init < 22) {
            pab[init] = 1;
            init++;
        }
        for (int i = 0; i < eventos.size(); i++) {
            for (int j = 0; j < 22; j++) {
                pab[j] = pab[j] * eventos.get(i).getProbabilidade()[j];
            }

        }
        return pab;
    }

    /**
     *
     * @param Pabxb
     * @return Retorna P(b)
     */
    private double Pb(double[] Pabxb) {
        double pb = 0;
        for (int i = 0; i < 22; i++) {
            pb = pb + Pabxb[i];
        }

        return pb;
    }
}
