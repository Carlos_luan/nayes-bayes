/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.nayve.bayes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author carlo
 */
public class Data_Warehouse {

    public void criar() {
        try {
            Connection connection = DriverManager.getConnection("jdbc:sqlite:data_warehouse.db");
            Statement comandoSql = connection.createStatement();
            comandoSql.execute("CREATE TABLE IF NOT EXISTS relatorio (\n"
                    + "    id       INTEGER         PRIMARY KEY AUTOINCREMENT,\n"
                    + "    decisao  VARCHAR (10000),\n"
                    + "    etiqueta VARCHAR (100),\n"
                    + "    Data     DATE\n"
                    + ");");

        } catch (SQLException ex) {
            Logger.getLogger(Data_Warehouse.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void editar(String func, String table, String Collum) {

    }

    public ResultSet select() {
        ResultSet resultset = null;
        try {
            Connection connection = DriverManager.getConnection("jdbc:sqlite:data_warehouse.db");
            PreparedStatement stmt;
            stmt = connection.prepareStatement("select decisao from relatorio");
            resultset = stmt.executeQuery();
            connection.clearWarnings();

        } catch (SQLException ex) {
            Logger.getLogger(Data_Warehouse.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultset;
    }
    
      public ResultSet selectFull() {
        ResultSet resultset = null;
        try {
            Connection connection = DriverManager.getConnection("jdbc:sqlite:data_warehouse.db");
            PreparedStatement stmt;
            stmt = connection.prepareStatement("select *from relatorio");
            resultset = stmt.executeQuery();
            connection.clearWarnings();
            return resultset;
        } catch (SQLException ex) {
            Logger.getLogger(Data_Warehouse.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(0);
        }
        return resultset;
    }
}
