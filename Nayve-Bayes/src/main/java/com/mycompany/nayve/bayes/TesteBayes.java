/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.nayve.bayes;

import Model.Probabilidades;
import Model.Termos;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author carlo
 */
public class TesteBayes {
//
//    String processo = "SENTENCA TIPO A\n"
//            + " HTML1\n"
//            + " EVENTO 102486375  06122019 153636  PROCEDENCIA  JULGADO PROCEDENTE O PEDIDO";
//
//    public boolean testeunico(Probabilidades Pa, List<Termos> DataSet) {
//        Bayes calculo = new Bayes();
//        double[] RBayes;
//        int etiqueta;
//        List<Termos> TEncontrados = PesquisadeChaves(DataSet, processo);
//        RBayes = calculo.calculo(Pa, TEncontrados);
//        etiqueta = decisao(RBayes);
//        if (etiqueta == 20) {
//            System.out.println("Acertou"+ etiqueta);
//            //salvo o indice afim de verificar acertividade
//        } else if (etiqueta == 23) {
//            System.out.println("não soube definir");
//        } else {
//            System.out.println("Errou: "+ etiqueta);
//        }
//        return true;
//    }

    public boolean testes(Probabilidades Pa, List<Termos> DataSet) {

        try {
            Bayes calculo = new Bayes();
            double[] RBayes;
            int etiqueta;
            Connection umbler = DriverManager.getConnection("jdbc:mysql://mysql742.umbler.com:41890/bayesifpa", "dattebayo", "MeuJeitoNinjadeSer");
            PreparedStatement stmt = umbler.prepareStatement("SELECT *FROM `bayesifpa`.`teste`;");
           // PreparedStatement stmt = umbler.prepareStatement("SELECT * FROM bayesifpa.teste where etiqueta =1;");
            ResultSet rst = stmt.executeQuery();
            double acertou = 0;
            double errou = 0;
            double ndef = 0;
            double total = 0;
            while (rst.next()) {

                //recebo a decisão                
                //envio para Bayes TEncontrados
                List<Termos> TEncontrados = PesquisadeChaves(DataSet, rst.getString("decisao").trim().replace(" ", "").replace("  ", ""));
                //Recebo o indice encontrado
                RBayes = calculo.calculo(Pa, TEncontrados);
                //Comparo com o indice que tem no banco
                etiqueta = decisao(RBayes);
//                if (etiqueta == 7) {
//                    etiqueta = 1;
//                }
                if (rst.getInt("etiqueta") == etiqueta) {
                    System.out.println(rst.getInt("id") + "Acertou: " + etiqueta);
                    acertou = acertou + 1;
//salvo o indice afim de verificar acertividade
                } else if (etiqueta == 23) {
                    System.out.println(rst.getInt("id") + "não soube definir");
                    ndef = ndef + 1;
                } else {
                    System.out.println(rst.getInt("id") + "\nErrou: " + etiqueta);
                    errou = errou + 1;
                }

                total = total + 1;
            }

            System.out.println("Acertos: " + acertou + " ->" + ((acertou / total) * 100) + "%");
            System.out.println("Erros: " + errou + " ->" + ((errou / total) * 100) + "%");
            System.out.println("Não soube definir: " + ndef + " ->" + ((ndef / total) * 100) + "%");
        } catch (SQLException ex) {
            Logger.getLogger(TesteBayes.class.getName()).log(Level.SEVERE, null, ex);
        }

        return true;
    }

    public List<Termos> PesquisadeChaves(List<Termos> DataSet, String Decisao) {
        List<Termos> TEncontrados = new ArrayList<>();

        for (int i = 0; i < DataSet.size(); i++) {
            if (Decisao.contains(DataSet.get(i).getKey())) {
                System.out.println(DataSet.get(i).getKey());
                TEncontrados.add(DataSet.get(i));
            }
        }
        return TEncontrados;
    }

    public int decisao(double[] RBayes) {
        double maior = 0;
        int indice = 23;
        System.out.print("Maior: " + maior);
//         for (int i=0;i<22;i++)
//         {
//             System.out.println(RBayes[i]);
//         }
        for (int i = 0; i < 22; i++) {
            // System.out.print(RBayes[i]+" || ");
            if (RBayes[i] > maior) {
                //  System.out.print("Maior: "+maior);
                //  System.out.println(i); 
                indice = i + 1;
            }
        }
        System.out.println(indice);
        return indice;
    }

}
