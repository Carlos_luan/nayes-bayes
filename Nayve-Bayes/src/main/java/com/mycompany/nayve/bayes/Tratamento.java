/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.nayve.bayes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Normalizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author carlo
 */
public class Tratamento {

    public String tratamento(String chave) {
        chave = chave.trim();// retiro espaços no inicio e no final do texto
     //   chave = Normalizer.normalize(chave, Normalizer.Form.NFD);
        chave = chave.replaceAll("[^\\p{ASCII}]", "");// retiro acentuação
        //  chave = chave.replaceAll("[^0-9a-zA-Z]+", " "); //retiro caracteres especiais
        chave = chave.replaceAll("[0-9]", "");
//        chave = chave.replace(",", " ");
//        chave = chave.replace(":", " ");
//        chave = chave.replace("-", " ");
//        chave = chave.replace("_", " ");
//        chave = chave.replace("\r", " ");
//        chave = chave.replace("\t", " ");
//        chave = chave.replace("\n", " ");
//        chave = chave.replace(";", " ");
        chave = chave.replace("DA(S)", " ");
        chave = chave.replace("DO(S)", " ");
//        chave = chave.replace("\\", " ");
//        chave = chave.replace("/", " ");
//        chave = chave.replace("(", " ");
//        chave = chave.replace(")", " ");
//        chave = chave.replace(".", " ");
//        chave = chave.replaceAll("   ", " ");  //retira tabulação
        chave = chave.trim();  //retira tabulação
        return chave;
    }
}
