/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.nayve.bayes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import Model.Termos;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author carlo
 */
public class Data_Bayes {

    Connection connection = null;

    public Statement conectar() {
        Connection connection = null;
        Statement comandoSql = null;
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:data_mining.db");
            comandoSql = connection.createStatement();

        } catch (SQLException ex) {
            Logger.getLogger(Data_Warehouse.class.getName()).log(Level.SEVERE, null, ex);
        }
        return comandoSql;
    }

    public void inserir(String chave) {
        Statement comandoSql = conectar();
        try {
            comandoSql.execute("INSERT INTO bayes (chave)\n"
                    + "                  VALUES ('" + chave + "');");
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(Data_Bayes.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    public void dropNomes() {
        try {
            ResultSet nomes = RetiraNomes();
            Statement conexao = conectar();
            while (nomes.next()) {
                conexao.execute("DELETE FROM bayes\n"
                        + "      WHERE chave = '" + nomes.getString("nomes") + "';");
            }

        } catch (SQLException ex) {
            Logger.getLogger(Data_Bayes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ResultSet RetiraNomes() {
        try {
            Connection conexao = DriverManager.getConnection("jdbc:sqlite:Lista_Nomes.db");
            Statement comandoSql = conexao.createStatement();
            ResultSet resultset = comandoSql.executeQuery("SELECT nomes FROM Nomes");
            return resultset;
        } catch (SQLException ex) {
            Logger.getLogger(Data_Bayes.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public boolean chaveOK(String chave) throws SQLException {
        ResultSet resultset = null;
        Statement comandoSql = null;
        comandoSql = conectar();
        int i = 0;
        try {
            resultset = comandoSql.executeQuery("SELECT chave from bayes where chave = '" + chave + "'");
        } catch (SQLException ex) {
            Logger.getLogger(Data_Bayes.class.getName()).log(Level.SEVERE, null, ex);
        }
        while (resultset.next()) {
            i++;
        }
        if (i > 0) {
            connection.close();
            return true;
        } else {
            connection.close();
            return false;
        }
    }

    //Passo 1: Capturar as chaves que estão no Servidor
    //Passo 2: Armazenar em um vetor de int
    public List<Termos> treinamento() {
        try {
            List<Termos> Knowledge = new ArrayList<>();
            Connection umbler = DriverManager.getConnection("jdbc:mysql://mysql742.umbler.com:41890/bayesifpa", "dattebayo", "MeuJeitoNinjadeSer");
            PreparedStatement stmt = umbler.prepareStatement("SELECT chave FROM bayesifpa.bayes;");
            ResultSet rst = stmt.executeQuery();
            while (rst.next()) {
                Termos temp = new Termos(rst.getString("chave"));
                Knowledge.add(temp);
            }
            stmt.close();
            umbler.close();
            return Knowledge;
        } catch (SQLException ex) {
            Logger.getLogger(Data_Bayes.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<Termos> chaves(List<Termos> knl) {
        try {
            int[] contador;
            Connection umbler = DriverManager.getConnection("jdbc:mysql://mysql742.umbler.com:41890/bayesifpa", "dattebayo", "MeuJeitoNinjadeSer");
            for (int i = 0; i < knl.size(); i++) {
                System.out.println(i + "|");
                contador = knl.get(i).getKnowledge();
                PreparedStatement stmt = umbler.prepareStatement("select count(etiqueta) as contador,etiqueta from relatorio where relatorio.decisao like ? group by etiqueta;");
                stmt.setString(1, "%" + knl.get(i).getKey() + "%");
                ResultSet rst = stmt.executeQuery();
                while (rst.next()) {
                    contador[rst.getInt("etiqueta") - 1] = rst.getInt("contador");
                }
                knl.get(i).setKnowledge(contador);
                stmt.close();
            }
            umbler.close();
            return knl;
        } catch (SQLException ex) {
            Logger.getLogger(Data_Bayes.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(0);
        }
        return null;
    }

    public void update(List<Termos> knl) {
        String SQL = "UPDATE `bayesifpa`.`bayes` SET\n"
                + "`1` = ?,\n"
                + "`2` = ?,\n"
                + "`3` = ?,\n"
                + "`4` = ?,\n"
                + "`5` = ?,\n"
                + "`6` = ?,\n"
                + "`7` = ?,\n"
                + "`8` = ?,\n"
                + "`9` = ?,\n"
                + "`10` = ?,\n"
                + "`11` = ?,\n"
                + "`12` = ?,\n"
                + "`13` = ?,\n"
                + "`14` = ?,\n"
                + "`15` = ?,\n"
                + "`16` = ?,\n"
                + "`17` = ?,\n"
                + "`18` = ?,\n"
                + "`19` = ?,\n"
                + "`20` = ?,\n"
                + "`21` = ?,\n"
                + "`22` = ? \n"
                + "WHERE `chave` = ?;";
        try {
            Connection umbler = DriverManager.getConnection("jdbc:mysql://mysql742.umbler.com:41890/bayesifpa", "dattebayo", "MeuJeitoNinjadeSer");
            for (int i = 0; i < knl.size(); i++) {
                PreparedStatement upt = umbler.prepareStatement(SQL);
                upt.setInt(1, knl.get(i).getKnowledge()[0]);
                upt.setInt(2, knl.get(i).getKnowledge()[1]);
                upt.setInt(3, knl.get(i).getKnowledge()[2]);
                upt.setInt(4, knl.get(i).getKnowledge()[3]);
                upt.setInt(5, knl.get(i).getKnowledge()[4]);
                upt.setInt(6, knl.get(i).getKnowledge()[5]);
                upt.setInt(7, knl.get(i).getKnowledge()[6]);
                upt.setInt(8, knl.get(i).getKnowledge()[7]);
                upt.setInt(9, knl.get(i).getKnowledge()[8]);
                upt.setInt(10, knl.get(i).getKnowledge()[9]);
                upt.setInt(11, knl.get(i).getKnowledge()[10]);
                upt.setInt(12, knl.get(i).getKnowledge()[11]);
                upt.setInt(13, knl.get(i).getKnowledge()[12]);
                upt.setInt(14, knl.get(i).getKnowledge()[13]);
                upt.setInt(15, knl.get(i).getKnowledge()[14]);
                upt.setInt(16, knl.get(i).getKnowledge()[15]);
                upt.setInt(17, knl.get(i).getKnowledge()[16]);
                upt.setInt(18, knl.get(i).getKnowledge()[17]);
                upt.setInt(19, knl.get(i).getKnowledge()[18]);
                upt.setInt(20, knl.get(i).getKnowledge()[19]);
                upt.setInt(21, knl.get(i).getKnowledge()[20]);
                upt.setInt(22, knl.get(i).getKnowledge()[21]);
                upt.setString(23, knl.get(i).getKey());
                upt.executeUpdate();
                upt.close();
            }
            umbler.close();
        } catch (SQLException ex) {
            Logger.getLogger(Data_Bayes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param knl
     * @return Retorna Lista de Termos com Eventos Registrados
     */
    public List<Termos> Eventos(List<Termos> knl) {
        try {
            int[] contador = new int[22];
            Connection umbler = DriverManager.getConnection("jdbc:mysql://mysql742.umbler.com:41890/bayesifpa", "dattebayo", "MeuJeitoNinjadeSer");
            PreparedStatement stmt = umbler.prepareStatement("SELECT * FROM bayesifpa.bayes;");
            ResultSet rst = stmt.executeQuery();
            while (rst.next()) {
                Termos temporario = new Termos(rst.getString("chave"));
                for (int j = 0; j < 22; j++) {
                    contador[j] = rst.getInt("" + (j+1) + "");
                }
                temporario.setKnowledge(contador);
                knl.add(temporario);
            }

            stmt.close();

            umbler.close();
            return knl;
        } catch (SQLException ex) {
            Logger.getLogger(Data_Bayes.class.getName()).log(Level.SEVERE, null, ex);
            // System.exit(0);
        }
        return null;
    }

    public double[] ProbabilidadeEvento() {
        double[] Pa = new double[22];
        double soma = 0;
        try {
            Connection umbler = DriverManager.getConnection("jdbc:mysql://mysql742.umbler.com:41890/bayesifpa", "dattebayo", "MeuJeitoNinjadeSer");
            PreparedStatement stmt = umbler.prepareStatement("SELECT etiqueta,count(etiqueta)FROM bayesifpa.relatorio Group By Etiqueta order by etiqueta asc;");
            ResultSet rst = stmt.executeQuery();
            System.out.println("\n");
            while (rst.next()) {
                soma = soma + rst.getDouble("count(etiqueta)");
                Pa[rst.getRow() - 1] = rst.getDouble("count(etiqueta)");
            }
            for (int i = 0; i < 22; i++) {
                Pa[i] = Pa[i] / soma;
                System.out.print(Pa[i] + " || ");
            }
            System.out.println("\n");
            return Pa;
        } catch (SQLException ex) {
            Logger.getLogger(Data_Bayes.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Pa;
    }

}
