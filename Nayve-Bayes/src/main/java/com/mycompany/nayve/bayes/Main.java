/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.nayve.bayes;

import Model.Probabilidades;
import Model.Termos;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author carlo
 */
public class Main {

    /**
     * @param args the command line arguments
     * @throws java.sql.SQLException
     */
    public static void main(String[] args) throws SQLException {
        // Data_Mining teste = new Data_Mining();
        Data_Bayes bayes = new Data_Bayes();
        TesteBayes testes = new TesteBayes();
        //teste.Splitter();
        List<Termos> knl = new ArrayList<>();
        /**
         * Recebo as palavras existentes dentro do dataset criado;
         */
        System.out.println("Finalizado Captura de Palavras");
        /**
         * bayes.chaves retorna a palavra utilizada com as suas devidas
         * ocorrencias e probabilidades já calculadas.
         */
        //knl = bayes.treinamento();
        //knl = bayes.chaves(knl);
        knl = bayes.Eventos(knl);

        System.out.println("Finalizado Captura de Repetições");
//      
//        for (int i = 0; i < knl.size(); i++) {
//            System.out.print(knl.get(i).getKey() + ": ");
//            for (int j = 0; j < knl.get(i).getKnowledge().length; j++) {
//                System.out.print(knl.get(i).getKnowledge()[j] + " | ");
//            }
//            System.out.print("\n");
//        }
      //  bayes.update(knl);
//        System.out.println("Finalizado Treinamento");
        //bayes.dropNomes();
        double[] Pa = bayes.ProbabilidadeEvento();
        Probabilidades PA = new Probabilidades();
        PA.setEventos(Pa);
        testes.testes(PA, knl);
        //  testes.testeunico(PA, knl);
    }

}
