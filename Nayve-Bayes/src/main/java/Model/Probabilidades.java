/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author carlo
 */
public class Probabilidades {

    double[] eventos = new double[22];
    double[] probabilidade = new double[22];

    public Probabilidades() {
    }

    public Probabilidades(double[] eventos) {
        for (int i = 0; i < 22; i++) {
            eventos[i] = 0;
        }
        calculo();
    }

    public void setProbabilidade(double[] probabilidade) {
        this.probabilidade = probabilidade;
    }

    private void calculo() {
        double total = 0;
        for (int i = 0; i < 22; i++) {
            total = total + eventos[i];
        }

        for (int i = 0; i < 22; i++) {
            probabilidade[i] = eventos[i] / total;
        }
    }

    public double[] getEventos() {
        return eventos;
    }

    public void setEventos(double[] eventos) {
        this.eventos = eventos;
        calculo();
    }

    public double[] getProbabilidade() {
        return probabilidade;
    }
}
