/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author carlo
 */
public class Termos {

    String Key;
    int[] Knowledge = new int[22];
    double[] probabilidade = new double[22];

    /**
     * @param Know - Chave: Palavra que é capturada dentro de processos
     * judiciais
     * @param Knowledge - Conhecimento: Indice de Ocorrencias dessa palavra
     * dentro de um tipo de classificação
     * @author carlo
     */
    public Termos(String Know) {
        this.Key = Know;
        for (int i = 0; i < 22; i++) {
            Knowledge[i] = 0;
        }
        //  calculo();
    }

    public double[] getProbabilidade() {
        return probabilidade;
    }

    public void setProbabilidade(double[] probabilidade) {
        this.probabilidade = probabilidade;
    }

    private void calculo() {
        double total = 0;

        for (int i = 0; i < 22; i++) {

            total = total + Knowledge[i];
        }
        if (total == 0) {
            total = 1;
        }
        System.out.println(Key + ": " + total + "\n");
        for (int i = 0; i < 22; i++) {

            probabilidade[i] = Knowledge[i] / total;
            //System.out.print(probabilidade[i] + " | ");
        }
    }

    public String getKey() {
        return Key;
    }

    public void setKey(String Key) {
        this.Key = Key;
    }

    public int[] getKnowledge() {
        return Knowledge;
    }

    public void setKnowledge(int[] Knowledge) {
        this.Knowledge = Knowledge;
        calculo();
    }

}
